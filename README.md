# VivaSpot - API De Propriedades de Spotippos.

Implementação da API de gerenciamento de propriedades de Spotippos.

## Tecnologias 

- java 8
- spring boot
- gradle
- mongodb
- docker

## Motivação

Projeto desenvolvido como solução para o desafio de propiedades do planeta de Spotippos.

## Instalação

Se não tiver/quiser instalar o mongodb. Poderá usar ele dockenizado.

Para a utilizar o mongodb dockenizado é necessário ter a [docker-machine](https://docs.docker.com/machine/get-started/) instalada e rodando.

### Instalando e inicializando o mongodb dockenizado:

```sh
$ ./scripts/create_db.sh
```

### Para iniciar o mongodb dockenizado:

```sh
$ ./scripts/start_db.sh
```

### Para desligar a mongodb dockenizado:

```sh
$ ./scripts/stop_db.sh
```

### Para importar as propriedades e províncias para o mongodb:

```sh
$ ./scripts/import_db.sh
```

### Para compilar e empacotar o app:

```sh
$ ./gradlew build
```

### Para rodar o app:

```sh
$ java -jar build/libs/vivaspot-0.1.0.jar
```

## Documentação da API

Para ver a documentação da API e/ou testa-la, considerando que esta rodando o app localmente, acesse http://localhost:8080/swagger-ui.html#/

### Exemplos de chamada:

Buscando uma propriedade

http://localhost:8080/properties/8

Criando uma propriedade
	
```sh
$ curl -X POST -H 'content-type: application/json' http://localhost:8080/properties -d '{"x":540, "y": 833, "title": "Imovel Novo", "price": 1250000, "description": "Imovel com 4 quartos e 2 banheiros", "beds": 4, "baths": 2, "squareMeters": 240}'
```

## Testes

### Para rodar os testes:

```sh
$ ./gradlew test
```  
  
Abs!  
Rodrigo Alves