mkdir -p ~/mongo-viva

docker pull mongo

docker network create network-viva

docker run --name mongo-viva --network=network-viva -p 27017:27017 -v ~/mongo-viva:/data/db -d mongo
