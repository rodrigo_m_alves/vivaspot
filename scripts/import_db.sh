#!/bin/bash

cd $(dirname "$0")

mongoimport --host localhost --port 27017 --db spotippos --collection provinces --file provinces.json --jsonArray

mongoimport --host localhost --port 27017 --db spotippos --collection temporary --file properties.json --jsonArray

mongo localhost:27017/spotippos migration.js
