db.temporary.find().forEach(function (document) {
    document._id = NumberLong(document.id)
    document.price = NumberLong(document.price)
    document.provinces = getProvinces(document)
    db.properties.insert(document)
})

db.temporary.drop()

db.properties.update({},{$unset: {id:1}},{multi: true})

function getProvinces(prop) {
  provinces = []

  db.provinces.find({
  'boundaries.upperLeft.x' :   {$lte: prop.lat},
  'boundaries.upperLeft.y' :   {$gte: prop.long},
  'boundaries.bottomRight.x' : {$gte: prop.lat},
  'boundaries.bottomRight.y' : {$lte: prop.long}
  },
  {'name' : 1, '_id' : 0}).forEach(function(document) {
    provinces.push(document.name);
  })

  return provinces;
}
