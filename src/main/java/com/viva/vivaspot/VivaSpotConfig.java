package com.viva.vivaspot;

public enum VivaSpotConfig {
	
	BASE_URL("vivaspot.base.url", "http://localhost:8080"), //
	
	MONGODB_HOST("vivaspot.mongo.host", "localhost"), //
	MONGODB_PORT("vivaspot.mongo.port", "27017"), //
	MONGODB_DATABASE("vivaspot.mongo.database", "spotippos");

	private final String key;
	private final String defaultValue;

	private VivaSpotConfig(String key, String defaultValue) {
		this.key = key;
		this.defaultValue = defaultValue;
	}

	public String getKey() {
		return key;
	}

	public String getStringValue() {
		return System.getProperty(key, defaultValue);
	}

	public Integer getIntegerValue() {
		return Integer.valueOf(getStringValue());
	}

}
