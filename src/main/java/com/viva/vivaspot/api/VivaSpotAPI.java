package com.viva.vivaspot.api;

import com.viva.vivaspot.domain.Properties;
import com.viva.vivaspot.domain.Property;
import com.viva.vivaspot.resource.request.PropertyRequest;
import com.viva.vivaspot.resource.response.PropertyResponse;

/**
 * Properties API
 * 
 * @author rmedeir
 *
 */
public interface VivaSpotAPI {

	/**
	 * create a {@Property}
	 * 
	 * @param request
	 * @return {@PropertyResponse}
	 */
	PropertyResponse createProperty(PropertyRequest request);

	/**
	 * get a property by id
	 * 
	 * @param id
	 * @return {@Property}
	 */
	Property getProperty(Long id);

	/**
	 * find properties by coordinates
	 * 
	 * @param ax
	 *            upper left latitude
	 * @param ay
	 *            upper left longitude
	 * @param bx
	 *            bottom right latitude
	 * @param by
	 *            bottom right longitude
	 * @return {@Properties}
	 */
	Properties getProperties(Integer ax, Integer ay, Integer bx, Integer by);
}
