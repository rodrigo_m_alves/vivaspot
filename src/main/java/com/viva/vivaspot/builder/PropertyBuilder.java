package com.viva.vivaspot.builder;

import java.util.List;

import com.viva.vivaspot.domain.Property;
import com.viva.vivaspot.resource.request.PropertyRequest;

public class PropertyBuilder {

	private Long id;
	private Integer x;
	private Integer y;
	private String title;
	private Long price;
	private String description;
	private Integer beds;
	private Integer baths;
	private Integer squareMeters;
	private List<String> provinces;
	
	public PropertyBuilder(PropertyRequest request) {
		this.x = Integer.valueOf(request.getX());
		this.y = Integer.valueOf(request.getY());
		this.title = request.getTitle();
		this.price = Long.valueOf(request.getPrice());
		this.description = request.getDescription();
		this.beds = Integer.valueOf(request.getBeds());
		this.baths = Integer.valueOf(request.getBaths());
		this.squareMeters = Integer.valueOf(request.getSquareMeters());
	}
	
	public PropertyBuilder create() {
		return this;
	}
	
	public PropertyBuilder addId(Long id) {
		this.id = id;
		return this;
	}
	
	public PropertyBuilder addX(Integer x) {
		this.x = x;
		return this;
	}
	
	public PropertyBuilder addY(Integer y) {
		this.y = y;
		return this;
	}
	
	public PropertyBuilder addTitle(String title) {
		this.title = title;
		return this;
	}
	
	public PropertyBuilder addPrice(Long price) {
		this.price = price;
		return this;
	}
	
	public PropertyBuilder addDescription(String description) {
		this.description = description;
		return this;
	}
	
	public PropertyBuilder addBeds(Integer beds) {
		this.beds = beds;
		return this;
	}
	
	public PropertyBuilder addBaths(Integer baths) {
		this.baths = baths;
		return this;
	}
	
	public PropertyBuilder addSquareMeters(Integer squareMeters) {
		this.squareMeters = squareMeters;
		return this;
	}
	
	public PropertyBuilder addProvinces(List<String> provinces) {
		this.provinces = provinces;
		return this;
	}
	
	public Property build() {
		return new Property(id, x, y, title, price, description, beds, baths, squareMeters, provinces);
	}
}
