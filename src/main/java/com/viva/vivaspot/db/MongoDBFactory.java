package com.viva.vivaspot.db;

import static com.viva.vivaspot.VivaSpotConfig.MONGODB_DATABASE;
import static com.viva.vivaspot.VivaSpotConfig.MONGODB_HOST;
import static com.viva.vivaspot.VivaSpotConfig.MONGODB_PORT;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import com.mongodb.MongoClient;

@Configuration
public class MongoDBFactory {

	@Bean
	public MongoDbFactory mongoDbFactory() throws Exception {
		MongoClient mongoClient = new MongoClient(MONGODB_HOST.getStringValue(), MONGODB_PORT.getIntegerValue());
		
		return new SimpleMongoDbFactory(mongoClient, MONGODB_DATABASE.getStringValue());
	}

	@Bean
	public MongoTemplate mongoTemplate() throws Exception {
		DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
        MappingMongoConverter mappingConverter = new MappingMongoConverter(dbRefResolver, new MongoMappingContext());
       
        mappingConverter.setTypeMapper(new DefaultMongoTypeMapper(null));
        
		return new MongoTemplate(mongoDbFactory(), mappingConverter);
	}

}
