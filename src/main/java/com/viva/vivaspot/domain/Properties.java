package com.viva.vivaspot.domain;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class Properties implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(required = true, value = "total number of properties on this condition")
	private final Integer totalProperties;
	
	@ApiModelProperty(value = "properties list")
	private final List<Property> properties;

	public Properties() {
		super();
		this.totalProperties = null;
		this.properties = null;
	}
	
	public Properties(Integer totalProperties, List<Property> properties) {
		super();
		this.totalProperties = totalProperties;
		this.properties = properties;
	}

	public Integer getTotalProperties() {
		return totalProperties;
	}

	public List<Property> getProperties() {
		return properties;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Properties [totalProperties=");
		builder.append(totalProperties);
		builder.append(", properties=");
		builder.append(properties);
		builder.append("]");
		return builder.toString();
	}
	
}
