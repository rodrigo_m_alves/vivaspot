package com.viva.vivaspot.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import io.swagger.annotations.ApiModelProperty;

@Document(collection = "properties")
public class Property implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ApiModelProperty(required = true, value = "property identifier")
	private final Long id;
	
	@Field("lat")
	@ApiModelProperty(required = true, value = "property latitude")
	private final Integer x;
	
	@Field("long")
	@ApiModelProperty(required = true, value = "property longitude")
	private final Integer y;
	
	@ApiModelProperty(required = true, value = "property title")
	private final String title;
	
	@ApiModelProperty(required = true, value = "property price")
	private final Long price;
	
	@ApiModelProperty(required = true, value = "property description")
	private final String description;
	
	@ApiModelProperty(required = true, value = "number of beds of property")
	private final Integer beds;
	
	@ApiModelProperty(required = true, value = "number of baths of property")
	private final Integer baths;
	
	@ApiModelProperty(required = true, value = "square meters of property")
	private final Integer squareMeters;
	
	@ApiModelProperty(required = true, value = "provinces of property")
	private final List<String> provinces;
	
	public Property() {
		this.id = null;
		this.x = null;
		this.y = null;
		this.title = null;
		this.price = null;
		this.description = null;
		this.beds = null;
		this.baths = null;
		this.squareMeters = null;
		this.provinces = null;
	}
	
	public Property(Long id, Integer x, Integer y, String title, Long price, String description, Integer beds,
			Integer baths, Integer squareMeters, List<String> provinces) {
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.title = title;
		this.price = price;
		this.description = description;
		this.beds = beds;
		this.baths = baths;
		this.squareMeters = squareMeters;
		this.provinces = provinces;
	}

	public Long getId() {
		return id;
	}

	public Integer getX() {
		return x;
	}

	public Integer getY() {
		return y;
	}

	public String getTitle() {
		return title;
	}

	public Long getPrice() {
		return price;
	}

	public String getDescription() {
		return description;
	}

	public Integer getBeds() {
		return beds;
	}

	public Integer getBaths() {
		return baths;
	}

	public Integer getSquareMeters() {
		return squareMeters;
	}
	
	public List<String> getProvinces() {
		return provinces;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((baths == null) ? 0 : baths.hashCode());
		result = prime * result + ((beds == null) ? 0 : beds.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((provinces == null) ? 0 : provinces.hashCode());
		result = prime * result + ((squareMeters == null) ? 0 : squareMeters.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Property other = (Property) obj;
		if (baths == null) {
			if (other.baths != null)
				return false;
		} else if (!baths.equals(other.baths))
			return false;
		if (beds == null) {
			if (other.beds != null)
				return false;
		} else if (!beds.equals(other.beds))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (provinces == null) {
			if (other.provinces != null)
				return false;
		} else if (!provinces.equals(other.provinces))
			return false;
		if (squareMeters == null) {
			if (other.squareMeters != null)
				return false;
		} else if (!squareMeters.equals(other.squareMeters))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (x == null) {
			if (other.x != null)
				return false;
		} else if (!x.equals(other.x))
			return false;
		if (y == null) {
			if (other.y != null)
				return false;
		} else if (!y.equals(other.y))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Property [id=");
		builder.append(id);
		builder.append(", x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append(", title=");
		builder.append(title);
		builder.append(", price=");
		builder.append(price);
		builder.append(", description=");
		builder.append(description);
		builder.append(", beds=");
		builder.append(beds);
		builder.append(", baths=");
		builder.append(baths);
		builder.append(", squareMeters=");
		builder.append(squareMeters);
		builder.append(", provinces=");
		builder.append(provinces);
		builder.append("]");
		return builder.toString();
	}

}
