package com.viva.vivaspot.exception;

public class PropertyNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final Long propertyId;
	
	public PropertyNotFoundException(Long id) {
		super();
		this.propertyId = id;
	}

	public PropertyNotFoundException(Long id, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.propertyId = id;
	}

	public PropertyNotFoundException(Long id, String message, Throwable cause) {
		super(message, cause);
		this.propertyId = id;
	}

	public PropertyNotFoundException(Long id, String message) {
		super(message);
		this.propertyId = id;
	}

	public PropertyNotFoundException(Long id, Throwable cause) {
		super(cause);
		this.propertyId = id;
	}

	public Long getPropertyId() {
		return propertyId;
	}

}
