package com.viva.vivaspot.exception;

public class ProvinceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private static final String MESSAGE = "province not found";
	
	public ProvinceNotFoundException() {
		super(MESSAGE);
	}

	public ProvinceNotFoundException(String message) {
		super(message);
	}

	
}
