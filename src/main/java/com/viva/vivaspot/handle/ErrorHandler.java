package com.viva.vivaspot.handle;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.viva.vivaspot.exception.DatabaseException;
import com.viva.vivaspot.exception.PropertyNotFoundException;
import com.viva.vivaspot.exception.ProvinceNotFoundException;
import com.viva.vivaspot.resource.response.Error;
import com.viva.vivaspot.resource.response.PropertyResponse;

@ControllerAdvice
public class ErrorHandler {

	private static final String PROPERTY_ID_NOT_FOUND = "Property id {} not found";

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public PropertyResponse processValidationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();

		return processFieldErrors(fieldErrors);
	}

	private PropertyResponse processFieldErrors(List<FieldError> fieldErrors) {
		List<Error> errors = new ArrayList<>();

		for (FieldError fieldError : fieldErrors) {
			errors.add(new Error(fieldError.getField(), fieldError.getDefaultMessage()));
		}

		return new PropertyResponse(errors);
	}

	@ExceptionHandler(PropertyNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public Error processPropertyNotFound(PropertyNotFoundException ex) {
		return new Error("id", getPropertyNotFoundMessage(ex));
	}

	private String getPropertyNotFoundMessage(PropertyNotFoundException ex) {
		return PROPERTY_ID_NOT_FOUND.replace("{}", String.valueOf(ex.getPropertyId()));
	}
	
	@ExceptionHandler(ProvinceNotFoundException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public Error processProvinceNotFound(PropertyNotFoundException ex) {
		return new Error(null,  ex.getMessage());
	}
	
	@ExceptionHandler(DatabaseException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public Error processProvinceNotFound(DatabaseException ex) {
		return new Error(null,  "internal server error");
	}

}
