package com.viva.vivaspot.repository;

import java.util.List;

import com.viva.vivaspot.domain.Property;

/**
 * Repository of {@Property}
 * 
 * @author rmedeir
 *
 */
public interface PropertyRepository {

	/**
	 * get the last id saved
	 * 
	 * @return last id
	 */
	Long getLastId();

	/**
	 * Save a {@Property}
	 * 
	 * @param {@Property}
	 * @return property id
	 */
	Long save(Property property);

	/**
	 * get a property by id
	 * 
	 * @param id
	 * @return {@Property}
	 */
	Property get(Long id);

	/**
	 * find properties by coordinates
	 * 
	 * @param ax
	 *            upper left latitude
	 * @param ay
	 *            upper left longitude
	 * @param bx
	 *            bottom right latitude
	 * @param by
	 *            bottom right longitude
	 * @return {@List<Property>}
	 */
	List<Property> getPropertiesByCoordinates(Integer ax, Integer ay, Integer bx, Integer by);
}
