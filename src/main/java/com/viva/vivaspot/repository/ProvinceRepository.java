package com.viva.vivaspot.repository;

import java.util.List;

/**
 * Repository of provinces
 * 
 * @author rmedeir
 *
 */
public interface ProvinceRepository {

	/**
	 * find the name of the province for a specific point
	 * 
	 * @param x
	 *            latitude
	 * @param y
	 *            longitude
	 * @return list of province names
	 */
	List<String> getProvinces(Integer x, Integer y);
}
