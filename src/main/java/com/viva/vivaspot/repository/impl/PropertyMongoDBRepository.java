package com.viva.vivaspot.repository.impl;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.viva.vivaspot.domain.Property;
import com.viva.vivaspot.exception.DatabaseException;
import com.viva.vivaspot.repository.PropertyRepository;

@Component
public class PropertyMongoDBRepository implements PropertyRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyMongoDBRepository.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;

	private AtomicLong seq;

	@Override
	public Long getLastId() {
		if (seq == null)
			setLastId();

		return seq.getAndIncrement();
	}

	@Override
	public Long save(Property property) {
		try {
			mongoTemplate.save(property);

			return property.getId();
		} catch (Exception ex) {
			LOGGER.error("database problem: {}", ex.getMessage());
			throw new DatabaseException(ex);
		}
	}

	@Override
	public Property get(Long id) {
		try {
			return mongoTemplate.findById(id, Property.class);
		} catch (Exception ex) {
			LOGGER.error("database problem: {}", ex.getMessage());
			throw new DatabaseException(ex);
		}
	}

	@Override
	public List<Property> getPropertiesByCoordinates(Integer ax, Integer ay, Integer bx, Integer by) {
		try {
			Query query = new Query();

			query.addCriteria(new Criteria() //
					.andOperator( //
							Criteria.where("lat").gte(ax), //
							Criteria.where("lat").lte(bx), //
							Criteria.where("long").lte(ay), //
							Criteria.where("long").gte(by)));

			List<Property> properties = mongoTemplate.find(query, Property.class);

			return properties;
		} catch (Exception ex) {
			LOGGER.error("database problem: {}", ex.getMessage());
			throw new DatabaseException(ex);
		}
	}

	private void setLastId() {
		Long lastId = 1L;
		Query query = new Query();

		query.with(new Sort(Sort.Direction.DESC, "_id"));

		Property property = mongoTemplate.findOne(query, Property.class);

		if (property != null)
			lastId = property.getId() + 1L;

		LOGGER.info("last id founded: {}", lastId);
		
		seq = new AtomicLong(lastId);
	}

}
