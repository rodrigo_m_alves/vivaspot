package com.viva.vivaspot.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCursor;
import com.viva.vivaspot.exception.DatabaseException;
import com.viva.vivaspot.repository.ProvinceRepository;

@Component
public class ProvinceMongoDBRepository implements ProvinceRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProvinceMongoDBRepository.class);

	private static final String COLLECTION = "provinces";

	@Autowired
	private MongoTemplate mongoTemplate;

	public ProvinceMongoDBRepository(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	
	@Override
	public List<String> getProvinces(Integer x, Integer y) {
		List<String> provinces = new ArrayList<>();

		try {

			BasicDBObject query = queryByCoordinates(x, y);

			DBCursor cursor = mongoTemplate.getCollection(COLLECTION).find(query);

			while (cursor.hasNext()) {
				BasicDBObject obj = (BasicDBObject) cursor.next();
				provinces.add(obj.getString("name"));
			}

		} catch (Exception ex) {
			LOGGER.error("database problem: {}", ex.getMessage());
			throw new DatabaseException(ex);
		}

		return provinces;
	}

	private BasicDBObject queryByCoordinates(Integer x, Integer y) {
		BasicDBObject query = new BasicDBObject();

		query.put("boundaries.upperLeft.x", BasicDBObjectBuilder.start("$lte", x).get());
		query.put("boundaries.upperLeft.y", BasicDBObjectBuilder.start("$gte", y).get());
		query.put("boundaries.bottomRight.x", BasicDBObjectBuilder.start("$gte", x).get());
		query.put("boundaries.bottomRight.y", BasicDBObjectBuilder.start("$lte", y).get());

		return query;
	}

}
