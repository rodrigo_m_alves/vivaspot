package com.viva.vivaspot.resource;

import static com.viva.vivaspot.resource.util.ResourceUtil.getResourceLinkWithID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.viva.vivaspot.api.VivaSpotAPI;
import com.viva.vivaspot.domain.Properties;
import com.viva.vivaspot.domain.Property;
import com.viva.vivaspot.resource.request.PropertyRequest;
import com.viva.vivaspot.resource.response.Link;
import com.viva.vivaspot.resource.response.PropertyResponse;
import com.viva.vivaspot.service.PropertyService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class VivaSpotResource implements VivaSpotAPI {

	private static final Logger LOGGER = LoggerFactory.getLogger(VivaSpotResource.class);
	
	private static final String PROPERTIES = "/properties";

	@Autowired
	private PropertyService propertyService;

	public VivaSpotResource(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
	
	@Override
	@ApiOperation(value = "create a new property")
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(path = PROPERTIES, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody PropertyResponse createProperty(@Valid @RequestBody PropertyRequest request) {
		LOGGER.debug("create property request: {}", request);

		Long id = propertyService.createProperty(request);

		LOGGER.info("a new property created. ID: {}", id);
		
		return new PropertyResponse(id, new Link(getResourceLinkWithID(id, PROPERTIES)));
	}

	@Override
	@ApiOperation(value = "get a property by ID")
	@GetMapping(path = "/properties/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Property getProperty(@ApiParam("property identifier") @PathVariable("id") Long id) {
		Property response = propertyService.getProperty(id);
		
		return response;
	}

	@Override
	@ApiOperation(value = "get a list of properties by coordinates")
	@GetMapping(path = PROPERTIES, produces = MediaType.APPLICATION_JSON_VALUE)
	public Properties getProperties(@ApiParam("upper left latitude") @RequestParam("ax") Integer ax, //
			@ApiParam("upper left longitude") @RequestParam("ay") Integer ay, //
			@ApiParam("bottom right latitude") @RequestParam("bx") Integer bx, //
			@ApiParam("bottom right longitude") @RequestParam("by") Integer by) {

		return propertyService.getPropertiesByCoordinates(ax, ay, bx, by);
	}

}
