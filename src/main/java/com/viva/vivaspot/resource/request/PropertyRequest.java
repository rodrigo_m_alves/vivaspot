package com.viva.vivaspot.resource.request;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class PropertyRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(required = true, value = "property latitude")
	private final String x;
	
	@ApiModelProperty(required = true, value = "property longitude")
	private final String y;
	
	@ApiModelProperty(required = true, value = "property title")
	private final String title;
	
	@ApiModelProperty(required = true, value = "property price")
	private final String price;
	
	@ApiModelProperty(required = true, value = "property description")
	private final String description;
	
	@ApiModelProperty(required = true, value = "number of beds of property")
	private final String beds;
	
	@ApiModelProperty(required = true, value = "number of baths of property")
	private final String baths;
	
	@ApiModelProperty(required = true, value = "square meters of property")
	private final String squareMeters;

	public PropertyRequest() {
		this.x = null;
		this.y = null;
		this.title = null;
		this.price = null;
		this.description = null;
		this.beds = null;
		this.baths = null;
		this.squareMeters = null;
	}
	
	public PropertyRequest(String x, String y, String title, String price, String description, String beds,
			String baths, String squareMeters) {
		super();
		this.x = x;
		this.y = y;
		this.title = title;
		this.price = price;
		this.description = description;
		this.beds = beds;
		this.baths = baths;
		this.squareMeters = squareMeters;
	}
	
	@NotNull(message = "x cannot be null")
	@Pattern(regexp = "[0-9]+", message = "x must be numeric")
	@Min(value = 0, message = "x must be greater or equal than 0")
	@Max(value = 1400, message = "x must be less or equal than 1400")
	public String getX() {
		return x;
	}

	@NotNull(message = "y cannot be null")
	@Pattern(regexp = "[0-9]+", message = "y must be numeric")
	@Min(value = 0, message = "y must be greater or equal than 0")
	@Max(value = 1000, message = "y must be less or equal than 1000")
	public String getY() {
		return y;
	}

	@NotBlank(message="title cannot be null")
	public String getTitle() {
		return title;
	}

	@NotNull(message="price cannot be null")
	@Pattern(regexp = "[0-9]+", message = "price must be numeric")
	public String getPrice() {
		return price;
	}

	@NotBlank(message="description cannot be null")
	public String getDescription() {
		return description;
	}

	@NotNull(message = "beds cannot be null")
	@Min(value = 1, message = "beds must be greater or equal than 1")
	@Max(value = 5, message = "beds must be less or equal than 5")
	@Pattern(regexp = "[0-9]+", message = "beds must be numeric")
	public String getBeds() {
		return beds;
	}

	@NotNull(message = "baths cannot be null")
	@Min(value = 1, message = "baths must be greater or equal than 1")
	@Max(value= 4, message = "baths must be less or equal than 4")
	@Pattern(regexp = "[0-9]+", message = "baths must be numeric")
	public String getBaths() {
		return baths;
	}

	@NotNull(message = "squareMeters cannot be null")
	@Min(value = 20, message = "squareMeters must be greater or equal than 20")
	@Max(value = 240, message = "squareMeters must be less or equal than 240")
	@Pattern(regexp = "[0-9]+", message = "squareMeters must be numeric")
	public String getSquareMeters() {
		return squareMeters;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PropertyRequest [x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append(", title=");
		builder.append(title);
		builder.append(", price=");
		builder.append(price);
		builder.append(", description=");
		builder.append(description);
		builder.append(", beds=");
		builder.append(beds);
		builder.append(", baths=");
		builder.append(baths);
		builder.append(", squareMeters=");
		builder.append(squareMeters);
		builder.append("]");
		return builder.toString();
	}
	
}
