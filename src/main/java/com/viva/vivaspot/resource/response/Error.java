package com.viva.vivaspot.resource.response;

import java.io.Serializable;

public class Error implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final String field;
	private final String message;
	
	public Error() {
		super();
		this.field = null;
		this.message = null;
	}
	
	public Error(String field, String message) {
		super();
		this.field = field;
		this.message = message;
	}

	public String getField() {
		return field;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Error [field=");
		builder.append(field);
		builder.append(", message=");
		builder.append(message);
		builder.append("]");
		return builder.toString();
	}
	
}
