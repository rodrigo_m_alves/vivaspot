package com.viva.vivaspot.resource.response;

import java.io.Serializable;

public class Link implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final String rel;
	private final String href;
	
	public Link() {
		this.rel = null;
		this.href = null;
	}
	
	public Link(String href) {
		this.rel = "self";
		this.href = href;
	}
	
	public Link(String rel, String href) {
		super();
		this.rel = rel;
		this.href = href;
	}
	
	public String getRel() {
		return rel;
	}
	
	public String getHref() {
		return href;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Link [rel=");
		builder.append(rel);
		builder.append(", href=");
		builder.append(href);
		builder.append("]");
		return builder.toString();
	}
	
}
