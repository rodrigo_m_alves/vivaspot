package com.viva.vivaspot.resource.response;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class PropertyResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "property identifier")
	private final Long id;
	
	@ApiModelProperty(value = "property link")
	private final Link link;
	
	@ApiModelProperty(value = "error list")
	private final List<Error> errors;
	
	public PropertyResponse() {
		this.id = null;
		this.link = null;
		this.errors = null;
	}
	
	public PropertyResponse(List<Error> errors) {
		this.id = null;
		this.link = null;
		this.errors = errors;
	}
	
	public PropertyResponse(Long id, Link link) {
		super();
		this.id = id;
		this.link = link;
		this.errors = null;
	}

	public Long getId() {
		return id;
	}

	public Link getLink() {
		return link;
	}
	
	public List<Error> getErrors() {
		return errors;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PropertyResponse [id=");
		builder.append(id);
		builder.append(", link=");
		builder.append(link);
		builder.append(", errors=");
		builder.append(errors);
		builder.append("]");
		return builder.toString();
	}

}
