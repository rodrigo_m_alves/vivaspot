package com.viva.vivaspot.resource.util;

import static com.viva.vivaspot.VivaSpotConfig.BASE_URL;

public class ResourceUtil {

	public static String getResourceLinkWithID(Long id, String resource) {
		return new StringBuilder(BASE_URL.getStringValue())
				.append(resource)
				.append("/")
				.append(id)
				.toString();
	}
	
}
