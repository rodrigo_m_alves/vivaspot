package com.viva.vivaspot.service;

import com.viva.vivaspot.domain.Properties;
import com.viva.vivaspot.domain.Property;
import com.viva.vivaspot.resource.request.PropertyRequest;

/**
 * Layer responsible for to manage the {@Property}
 * 
 * @author rmedeir
 *
 */
public interface PropertyService {

	/**
	 * create a {@Property}
	 * 
	 * @param 
	 * 		{@PropertyRequest}
	 * @return
	 * 		property identifier
	 */		
	Long createProperty(PropertyRequest request);

	/**
	 * get a property by id
	 * 
	 * @param 
	 * 		id
	 * @return 
	 * 		{@Property}
	 */
	Property getProperty(Long id);

	/**
	 * find properties by coordinates
	 * 
	 * @param ax
	 *            upper left latitude
	 * @param ay
	 *            upper left longitude
	 * @param bx
	 *            bottom right latitude
	 * @param by
	 *            bottom right longitude
	 * @return {@Properties}
	 */
	Properties getPropertiesByCoordinates(Integer ax, Integer ay, Integer bx, Integer by);

}
