package com.viva.vivaspot.service.impl;

import static java.lang.Integer.*;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viva.vivaspot.builder.PropertyBuilder;
import com.viva.vivaspot.domain.Properties;
import com.viva.vivaspot.domain.Property;
import com.viva.vivaspot.exception.PropertyNotFoundException;
import com.viva.vivaspot.exception.ProvinceNotFoundException;
import com.viva.vivaspot.repository.PropertyRepository;
import com.viva.vivaspot.repository.ProvinceRepository;
import com.viva.vivaspot.resource.request.PropertyRequest;
import com.viva.vivaspot.service.PropertyService;

@Service
public class PropertyServiceImpl implements PropertyService {

	@Autowired
	private PropertyRepository propertyRepository;

	@Autowired
	private ProvinceRepository provinceRepository;

	public PropertyServiceImpl(PropertyRepository propertyRepository, ProvinceRepository provinceRepository) {
		super();
		this.propertyRepository = propertyRepository;
		this.provinceRepository = provinceRepository;
	}

	@Override
	public Long createProperty(PropertyRequest request) {
		List<String> provinces = provinceRepository.getProvinces(valueOf(request.getX()), valueOf(request.getY()));

		if (provinces.isEmpty()) {
			throw new ProvinceNotFoundException();
		}

		Long id = propertyRepository.getLastId();

		Property property = new PropertyBuilder(request) //
				.create() //
				.addId(id) //
				.addProvinces(provinces) //
				.build();

		return propertyRepository.save(property);
	}

	@Override
	public Property getProperty(Long id) {
		Property property = propertyRepository.get(id);

		if (property == null) {
			throw new PropertyNotFoundException(id);
		}

		return property;
	}

	@Override
	public Properties getPropertiesByCoordinates(Integer ax, Integer ay, Integer bx, Integer by) {
		List<Property> properties = propertyRepository.getPropertiesByCoordinates(ax, ay, bx, by);
		return new Properties(properties.size(), properties);
	}

}
