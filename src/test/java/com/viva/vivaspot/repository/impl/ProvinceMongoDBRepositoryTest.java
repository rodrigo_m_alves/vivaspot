package com.viva.vivaspot.repository.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.mongodb.MongoException;
import com.viva.vivaspot.exception.DatabaseException;
import com.viva.vivaspot.repository.ProvinceRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProvinceMongoDBRepositoryTest {

	@Mock
	private MongoTemplate mongoTemplate;

	private ProvinceRepository provinceRepository;
	
	@Before
	public void setUp() {
		provinceRepository = new ProvinceMongoDBRepository(mongoTemplate);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DatabaseException.class)
	public void mongoDBFailure() {
		when(mongoTemplate.getCollection(any())).thenThrow(MongoException.class);

		provinceRepository.getProvinces(10, 20);
	}

}
