package com.viva.vivaspot.resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.viva.vivaspot.api.VivaSpotAPI;
import com.viva.vivaspot.domain.Properties;
import com.viva.vivaspot.domain.Property;
import com.viva.vivaspot.exception.PropertyNotFoundException;
import com.viva.vivaspot.resource.request.PropertyRequest;
import com.viva.vivaspot.resource.response.PropertyResponse;
import com.viva.vivaspot.service.PropertyService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class VivaSpotResourceTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Mock
	private PropertyService propertyService;
	
	private VivaSpotAPI vivaSpotAPI;

	@Before
	public void setUp() {
		vivaSpotAPI = new VivaSpotResource(propertyService);
	}

	@Test
	public void createPropertySuccess() throws Exception {
		ResponseEntity<PropertyResponse> response = restTemplate.postForEntity("/properties",
				createPropertyRequestValid(), PropertyResponse.class);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());

		assertNull(response.getBody().getErrors());
		assertNotNull(response.getBody().getId());
		assertNotNull(response.getBody().getLink());
	}

	@Test
	public void createPropertyValidationFailure() throws Exception {
		ResponseEntity<PropertyResponse> response = restTemplate.postForEntity("/properties",
				createPropertyRequestInvalid(), PropertyResponse.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

		assertNotNull(response.getBody().getErrors());
		assertNull(response.getBody().getId());
		assertNull(response.getBody().getLink());
	}
	
	@Test
	public void getPropertySuccess() {
		Long id = 10L;
		
		when(propertyService.getProperty(id)).thenReturn(getProperty(id));
		
		Property property = vivaSpotAPI.getProperty(id);
		
		assertNotNull(property);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PropertyNotFoundException.class)
	public void getPropertyNotFoundFailure() {
		Long id = 999999L;
		
		when(propertyService.getProperty(id)).thenThrow(PropertyNotFoundException.class);
		
		vivaSpotAPI.getProperty(id);
	}
	
	@Test
	public void getPropertiesSuccess() {
		when(propertyService.getPropertiesByCoordinates(0, 200, 200, 0)).thenReturn(getProperties());
		Properties properties = vivaSpotAPI.getProperties(0, 200, 200, 0);
		
		assertEquals(properties.getTotalProperties().intValue(), properties.getProperties().size());
		assertEquals(properties.getTotalProperties().intValue(), 1);
	}
	
	@Test
	public void getPropertiesNotFoundSuccessSuccess() {
		when(propertyService.getPropertiesByCoordinates(10000, 10000, 10000, 10000)).thenReturn(getPropertiesNotFound());
		Properties properties = vivaSpotAPI.getProperties(10000, 10000, 10000, 10000);
		
		assertEquals(properties.getTotalProperties().intValue(), properties.getProperties().size());
		assertEquals(properties.getTotalProperties().intValue(), 0);
	}
	
	private Properties getPropertiesNotFound() {
		return new Properties(0, Collections.emptyList());
	}

	private Properties getProperties() {
		List<Property> propers = new ArrayList<>();
		propers.add(getProperty(10L));
		
		return new Properties(propers.size(), propers);
	}

	private Property getProperty(long id) {
		return new Property(id, 100, 100, "title", 5000000L, "description", 2, 2, 50, Collections.emptyList());
	}

	private PropertyRequest createPropertyRequestValid() {
		return new PropertyRequest("8", "20", "title", "10000", "description", "2", "3", "200");
	}

	private PropertyRequest createPropertyRequestInvalid() {
		return new PropertyRequest("8", "20", "title", "10000", "description", "2", "3", "500");
	}
}
