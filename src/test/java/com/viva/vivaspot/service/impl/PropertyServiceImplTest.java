package com.viva.vivaspot.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.viva.vivaspot.domain.Properties;
import com.viva.vivaspot.domain.Property;
import com.viva.vivaspot.exception.PropertyNotFoundException;
import com.viva.vivaspot.exception.ProvinceNotFoundException;
import com.viva.vivaspot.repository.PropertyRepository;
import com.viva.vivaspot.repository.ProvinceRepository;
import com.viva.vivaspot.resource.request.PropertyRequest;
import com.viva.vivaspot.service.PropertyService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PropertyServiceImplTest {

	@Mock
	private PropertyRepository propertyRepository;

	@Mock
	private ProvinceRepository provinceRepository;

	private PropertyService propertyService;

	@Before
	public void setUp() {
		propertyService = new PropertyServiceImpl(propertyRepository, provinceRepository);
	}

	@Test(expected = ProvinceNotFoundException.class)
	public void provinceNotFoundFailure() {
		PropertyRequest request = getPropertyRequest();

		when(provinceRepository.getProvinces(Integer.valueOf(request.getX()), Integer.valueOf(request.getY())))
				.thenReturn(new ArrayList<>());

		propertyService.createProperty(request);
	}

	@Test
	public void createSuccess() {
		Long lastId = 123L;
		PropertyRequest request = getPropertyRequest();

		when(provinceRepository.getProvinces(Integer.valueOf(request.getX()), Integer.valueOf(request.getY())))
				.thenReturn(getProvince());

		when(propertyRepository.getLastId()).thenReturn(lastId);

		when(propertyRepository.save(getProperty(lastId))).thenReturn(lastId);

		Long returnedId = propertyService.createProperty(request);

		assertEquals(lastId, returnedId);

	}

	@Test
	public void getPropertySuccess() {
		Long id = 123L;
		Property property = getProperty(id);

		when(propertyRepository.get(id)).thenReturn(property);

		Property propertyResp = propertyService.getProperty(id);

		assertEquals(id, propertyResp.getId());
	}

	@Test(expected = PropertyNotFoundException.class)
	public void propertyNotFound() {
		Long id = 123L;

		when(propertyRepository.get(id)).thenReturn(null);

		propertyService.getProperty(id);
	}

	@Test
	public void resultWithoutProperties() {
		when(propertyRepository.getPropertiesByCoordinates(1, 1, 1, 1)).thenReturn(new ArrayList<>());

		Properties properties = propertyService.getPropertiesByCoordinates(1, 1, 1, 1);
		
		assertEquals(0, properties.getTotalProperties().intValue());
		assertEquals(0, properties.getProperties().size());
	}

	private Property getProperty(Long id) {
		return new Property(id, 50, 20, "title", 50000L, "description", 3, 3, 100, getProvince());
	}

	private List<String> getProvince() {
		List<String> provinces = new ArrayList<>();
		provinces.add("Groola");

		return provinces;
	}

	private PropertyRequest getPropertyRequest() {
		return new PropertyRequest("50", "20", "title", "50000", "description", "3", "3", "100");
	}

}
